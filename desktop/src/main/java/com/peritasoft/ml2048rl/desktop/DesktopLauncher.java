package com.peritasoft.ml2048rl.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.peritasoft.ml2048rl.MyLittle2048RogueLite;

public class DesktopLauncher {
    public static void main(String[] args) {
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle("My Little 2048-roguelite");
        config.setWindowedMode(640, 640);
        config.setWindowIcon(
                "icon128.png",
                "icon32.png",
                "icon16.png"
        );
        new Lwjgl3Application(new MyLittle2048RogueLite(), config);
    }
}
