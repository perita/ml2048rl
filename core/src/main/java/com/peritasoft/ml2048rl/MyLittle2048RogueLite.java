package com.peritasoft.ml2048rl;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Queue;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.ml2048rl.core.Direction;
import com.peritasoft.ml2048rl.core.Room;
import com.peritasoft.ml2048rl.core.RoomState;
import com.peritasoft.ml2048rl.graphics.RoomDrawer;

public class MyLittle2048RogueLite implements ApplicationListener, InputProcessor, GestureDetector.GestureListener {
    private final Queue<Direction> directions = new Queue<>();
    private RoomDrawer drawer;
    private Room room;
    private Viewport viewport;
    private SpriteBatch batch;
    private GestureDetector gestureDetector;

    @Override
    public void create() {
        batch = new SpriteBatch();
        viewport = new FitViewport(160f, 160f);
        createRoom();
        gestureDetector = new GestureDetector(this);
        Gdx.input.setInputProcessor(this);
    }

    private void createRoom() {
        room = new Room(6, 6);
        drawer = new RoomDrawer(room);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void render() {
        float delta = Gdx.graphics.getDeltaTime();
        drawer.act(delta);
        if (drawer.isIdle()) {
            if (room.state == RoomState.IDLE) {
                nextDirection();
            } else {
                room.act();
                drawer.refresh();
            }
        }
        viewport.apply(true);
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();
        drawer.draw(batch);
        batch.end();
    }

    private void nextDirection() {
        if (directions.isEmpty()) return;
        Direction direction = directions.removeFirst();
        if (room.move(direction)) {
            drawer.refresh();
        }
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        drawer.dispose();
        batch.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.ENTER:
                if (room.state == RoomState.GAMEOVER) {
                    reset();
                    return true;
                }
                break;
            case Input.Keys.UP:
            case Input.Keys.K:
            case Input.Keys.W:
                directions.addLast(Direction.NORTH);
                return true;
            case Input.Keys.LEFT:
            case Input.Keys.H:
            case Input.Keys.A:
                directions.addLast(Direction.WEST);
                return true;
            case Input.Keys.DOWN:
            case Input.Keys.J:
            case Input.Keys.S:
                directions.addLast(Direction.SOUTH);
                return true;
            case Input.Keys.RIGHT:
            case Input.Keys.L:
            case Input.Keys.D:
                directions.addLast(Direction.EAST);
                return true;
        }
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        if (room.state == RoomState.GAMEOVER) {
            reset();
            return true;
        }
        return false;
    }


    private void reset() {
        directions.clear();
        createRoom();
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        if (Math.abs(velocityX) > Math.abs(velocityY)) {
            directions.addLast(velocityX > 0 ? Direction.EAST : Direction.WEST);
        } else {
            directions.addLast(velocityY > 0 ? Direction.SOUTH : Direction.NORTH);
        }
        return true;
    }


    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return gestureDetector.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return gestureDetector.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return gestureDetector.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return gestureDetector.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return gestureDetector.scrolled(amountX, amountY);
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
