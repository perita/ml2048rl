package com.peritasoft.ml2048rl.core;

public class Goblin extends Actor implements Actor.Visitor {
    public Goblin(int value) {
        super(Actor.deriveValue(value));
    }

    @Override
    void interact(Actor other) {
        other.visit(this);
    }

    @Override
    public void visit(Actor.Visitor visitor) {
        visitor.accept(this);
    }

    @Override
    public void accept(Altar altar) {
        attack(altar);
    }

    @Override
    public void accept(Goblin goblin) {
        // Nothing to do; it is a differently valuated goblin
    }

    @Override
    public void accept(Gold gold) {
        // Nothing to do
    }

    @Override
    public void accept(Hero hero) {
        attack(hero);
    }

    @Override
    public void accept(Potion potion) {
        // Nothing to do
    }

    @Override
    public void accept(Rat rat) {
        attack(rat);
    }

    @Override
    public void accept(Spider spider) {
        attack(spider);
    }
}
