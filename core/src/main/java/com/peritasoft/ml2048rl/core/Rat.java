package com.peritasoft.ml2048rl.core;

public class Rat extends Actor implements Actor.Visitor {
    public Rat(int value) {
        super(Actor.deriveValue(value));
    }

    @Override
    void interact(Actor other) {
        other.visit(this);
    }

    @Override
    public void visit(Visitor visitor) {
        visitor.accept(this);
    }

    @Override
    public void accept(Altar altar) {
        attack(altar);
    }

    @Override
    public void accept(Goblin goblin) {
        attack(goblin);
    }

    @Override
    public void accept(Gold gold) {
        // Nothing to do
    }

    @Override
    public void accept(Hero hero) {
        attack(hero);
    }

    @Override
    public void accept(Potion potion) {
        // Nothing to do
    }

    @Override
    public void accept(Rat rat) {
        // Nothing to do; it is a differently valuated rat
    }

    @Override
    public void accept(Spider spider) {
        attack(spider);
    }
}
