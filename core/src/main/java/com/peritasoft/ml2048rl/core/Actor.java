package com.peritasoft.ml2048rl.core;

import com.badlogic.gdx.math.MathUtils;

public abstract class Actor {
    public final Room.Position position = new Room.Position();
    public final Room.Position prevPosition = new Room.Position();
    public final boolean movable;
    public int value;
    public Actor mergedWith;
    public boolean justAdded = true;
    public Room room;
    public boolean attacked;

    Actor() {
        this(true, MathUtils.randomBoolean(0.9f) ? 1 : 2);
    }

    Actor(int value) {
        this(true, value);
    }

    Actor(boolean movable, int value) {
        this.movable = movable;
        this.value = Math.max(1, value);
    }

    protected static int deriveValue(int value) {
        return value + (MathUtils.randomBoolean(0.9f) ? MathUtils.random(-1, 0) : 1);
    }

    void prepareToMove() {
        prevPosition.set(position);
        mergedWith = null;
        attacked = false;
        justAdded = false;
    }

    boolean canMergeWith(Actor other) {
        if (mergedWith != null) return false;
        if (this == other) return false;
        if (getClass() != other.getClass()) return false;
        return value == other.value;
    }

    void mergeWith(Actor other) {
        value++;
        mergedWith = other;
    }

    void place(Room room, int row, int col) {
        this.room = room;
        this.position.set(row, col);
    }

    void remove() {
        this.room = null;
    }

    protected int attack(Actor other) {
        int points = other.value;
        other.value = Math.max(0, other.value - value);
        other.attacked = true;
        if (other.value == 0) {
            other.removeFromRoom();
        }
        return points * (int) Math.pow(2, points);
    }

    protected void replace(Actor actor) {
        actor.removeFromRoom();
        room.move(actor.position.row, actor.position.col, position.row, position.col);
    }

    protected void removeFromRoom() {
        if (room == null) return;
        room.removeActor(position.row, position.col);
    }

    abstract void interact(Actor other);

    public abstract void visit(Visitor visitor);

    public interface Visitor {
        void accept(Altar altar);

        void accept(Goblin goblin);

        void accept(Gold gold);

        void accept(Hero hero);

        void accept(Potion potion);

        void accept(Rat rat);

        void accept(Spider spider);
    }
}
