package com.peritasoft.ml2048rl.core;

public class Hero extends Actor implements Actor.Visitor {

    @Override
    public void interact(Actor other) {
        other.visit(this);
    }

    @Override
    public void accept(Altar altar) {
        room.score += attack(altar);
    }

    @Override
    public void accept(Goblin goblin) {
        room.score += attack(goblin);
    }

    @Override
    public void accept(Gold gold) {
        room.score += (int) Math.pow(2, gold.value * gold.value);
        replace(gold);
    }

    @Override
    public void accept(Hero hero) {
        // Nothing to do; it should not happen
    }

    @Override
    public void accept(Potion potion) {
        value += potion.value;
        replace(potion);
    }

    @Override
    public void accept(Rat rat) {
        room.score += attack(rat);
    }

    @Override
    public void accept(Spider spider) {
        room.score += attack(spider);
    }

    @Override
    public void visit(Visitor visitor) {
        visitor.accept(this);
    }
}
