package com.peritasoft.ml2048rl.core;

public enum Direction {
    NORTH(0, 0, 1),
    WEST(2, -1, 0),
    SOUTH(4, 0, -1),
    EAST(6, 1, 0);

    public final int index;
    public final int dx;
    public final int dy;

    Direction(int index, int dx, int dy) {
        this.index = index;
        this.dx = dx;
        this.dy = dy;
    }
}
