package com.peritasoft.ml2048rl.core;

import com.badlogic.gdx.math.MathUtils;

import java.util.Iterator;
import java.util.function.Consumer;

public class Room {
    public final int cols;
    public final int rows;
    public final Hero hero;
    public Direction facing = Direction.NORTH;
    public final Position farthest = new Position();
    public final Position next = new Position();
    public int turn = 0;
    private final Actor[][] actors;
    public int score = 0;
    public RoomState state = RoomState.INVOKING;
    private Consumer<Room> interactFunct;

    public Room(int cols, int rows) {
        this.cols = cols;
        this.rows = rows;
        actors = new Actor[rows][cols];
        hero = new Hero();
        placeActor(hero, rows / 2, cols / 2);
        placeRandomActor();
    }

    public void act() {
        switch (state) {
            case IDLE:
            case GAMEOVER:
                // Nothing to do
                break;
            case MOVING:
                interactFunct.accept(this);
                break;
            case INTERACTING:
                placeRandomActor();
                state = RoomState.INVOKING;
                break;
            case INVOKING:
                turn++;
                if (hero.value == 0) {
                    state = RoomState.GAMEOVER;
                } else {
                    state = RoomState.IDLE;
                }
                break;
        }
    }

    private void placeRandomActor() {
        if (isFull()) {
            return; // shrug
        }
        findNextRandomPosition();
        Actor actor = getRandomActor();
        placeActor(actor, next.row, next.col);
    }

    private Actor getRandomActor() {
        if (MathUtils.random() <= 0.8) {
            switch (MathUtils.random(1)) {
                case 1:
                    return new Spider(hero.value);
                case 2:
                    return new Rat(hero.value);
                default:
                    return new Goblin(hero.value);
            }
        } else {
            switch (MathUtils.random(1)) {
                case 1:
                    return new Altar(hero.value);
                case 2:
                    return new Gold();
                default:
                    return new Potion();
            }
        }
    }

    private boolean isFull() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (actors[row][col] == null) {
                    return false;
                }
            }
        }
        return true;
    }

    private void findNextRandomPosition() {
        do {
            next.row = MathUtils.random(rows - 1);
            next.col = MathUtils.random(cols - 1);
        } while (actors[next.row][next.col] != null);
    }

    private void placeActor(Actor actor, int row, int col) {
        actors[row][col] = actor;
        actor.place(this, row, col);
    }

    void removeActor(int row, int col) {
        Actor actor = actors[row][col];
        if (actor == null) return;
        actor.remove();
        actors[row][col] = null;
    }

    public boolean moveNorth() {
        return move(Direction.NORTH, rows - 2, -1, 0, cols, Room::interactNorth);
    }

    public boolean moveWest() {
        return move(Direction.WEST, 0, rows, 1, cols, Room::interactWest);
    }

    public boolean moveSouth() {
        return move(Direction.SOUTH, 1, rows, 0, cols, Room::interactSouth);
    }

    public boolean moveEast() {
        return move(Direction.EAST, 0, rows, cols - 2, -1, Room::interactEast);
    }

    public boolean move(Direction direction) {
        switch (direction) {
            case NORTH:
                return moveNorth();
            case WEST:
                return moveWest();
            case SOUTH:
                return moveSouth();
            case EAST:
                return moveEast();
        }
        return false;
    }


    private boolean move(Direction direction, int rowBegin, int rowEnd, int colBegin, int colEnd, Consumer<Room> interactFunct) {
        if (state != RoomState.IDLE) return false;
        state = RoomState.MOVING;
        this.interactFunct = interactFunct;
        facing = direction;
        getActors().forEach(Actor::prepareToMove);
        final int rowDir = rowEnd > rowBegin ? 1 : -1;
        final int colDir = colEnd > colBegin ? 1 : -1;
        for (int row = rowBegin; row != rowEnd; row += rowDir) {
            for (int col = colBegin; col != colEnd; col += colDir) {
                Actor actor = actors[row][col];
                if (actor == null || !actor.movable) continue;
                findFarthestPositionFrom(row, col, direction);
                if (isNextWithinLimits() && actor.canMergeWith(nextActor())) {
                    nextActor().mergeWith(actor);
                    removeActor(row, col);
                } else {
                    move(farthest.row, farthest.col, row, col);
                }
            }
        }
        return true;
    }

    private void interactNorth() {
        interact(rows - 2, -1, 0, cols);
    }

    private void interactWest() {
        interact(0, rows, 1, cols);
    }

    private void interactSouth() {
        interact(1, rows, 0, cols);
    }

    private void interactEast() {
        interact(0, rows, cols - 2, -1);
    }

    private void interact(int rowBegin, int rowEnd, int colBegin, int colEnd) {
        if (state != RoomState.MOVING) return;
        state = RoomState.INTERACTING;
        getActors().forEach(Actor::prepareToMove);
        final int rowDir = rowEnd > rowBegin ? 1 : -1;
        final int colDir = colEnd > colBegin ? 1 : -1;
        for (int row = rowBegin; row != rowEnd; row += rowDir) {
            for (int col = colBegin; col != colEnd; col += colDir) {
                Actor actor = actors[row][col];
                if (actor == null) continue;
                next.set(row + facing.dy, col + facing.dx);
                if (!isNextWithinLimits()) continue;
                Actor other = nextActor();
                if (other == null) continue;
                actor.interact(other);
                if (actors[row][col] == null) {
                    shiftActors(row, col);
                    col -= colDir;
                }
            }
        }
    }

    private void shiftActors(int row, int col) {
        row -= facing.dy;
        col -= facing.dx;
        while (row >= 0 && row < rows && col >= 0 && col < cols) {
            Actor actor = actors[row][col];
            if (actor == null || !actor.movable) return;
            move(row + facing.dy, col + facing.dx, row, col);
            row -= facing.dy;
            col -= facing.dx;
        }
    }


    private Actor nextActor() {
        return actors[next.row][next.col];
    }

    private void findFarthestPositionFrom(int row, int col, Direction direction) {
        farthest.set(row, col);
        next.set(row + direction.dy, col + direction.dx);
        while (isNextWithinLimits() &&
                actors[next.row][next.col] == null) {
            farthest.set(next);
            next.advance(direction);
        }
    }

    private boolean isNextWithinLimits() {
        return next.row >= 0 && next.row < rows &&
                next.col >= 0 && next.col < cols;
    }


    void move(int toRow, int toCol, int fromRow, int fromCol) {
        if (toRow == fromRow && toCol == fromCol) return;
        if (actors[fromRow][fromCol] == null) return;
        Actor actor = actors[fromRow][fromCol];
        removeActor(fromRow, fromCol);
        placeActor(actor, toRow, toCol);
    }

    public Iterable<Actor> getActors() {
        return new ActorIterable(this);
    }

    public static class Position {
        public int col;
        public int row;

        public void set(int row, int col) {
            this.row = row;
            this.col = col;
        }

        public void set(Position other) {
            this.row = other.row;
            this.col = other.col;
        }

        public void advance(Direction direction) {
            row += direction.dy;
            col += direction.dx;
        }
    }

    public static class ActorIterable implements Iterable<Actor> {
        private final Room room;

        ActorIterable(Room room) {
            this.room = room;
        }

        public Iterator<Actor> iterator() {
            return new ActorIterator(room);
        }
    }

    public static class ActorIterator implements Iterator<Actor> {
        private final Room room;
        private Actor nextActor;

        ActorIterator(Room room) {
            this.room = room;
            this.nextActor = findNext();
        }

        @Override
        public boolean hasNext() {
            return nextActor != null;
        }

        @Override
        public Actor next() {
            Actor actor = nextActor;
            nextActor = actor == null ? null : findNext();
            return actor;
        }

        private Actor findNext() {
            final int startRow = nextActor == null ? 0 : nextActor.position.row;
            for (int row = startRow; row < room.rows; row++) {
                final int startCol = row > startRow || nextActor == null ? 0 : nextActor.position.col + 1;
                for (int col = startCol; col < room.cols; col++) {
                    Actor actor = room.actors[row][col];
                    if (actor != null) return actor;
                }
            }
            return null;
        }
    }
}
