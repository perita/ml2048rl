package com.peritasoft.ml2048rl.core;

public enum RoomState {
    IDLE,
    MOVING,
    INTERACTING,
    INVOKING,
    GAMEOVER
}
