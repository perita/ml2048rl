package com.peritasoft.ml2048rl.graphics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.peritasoft.ml2048rl.core.Direction;

interface ActorDrawer {
    void draw(Batch batch, float x, float y, Direction facing, float stateTime);
}
