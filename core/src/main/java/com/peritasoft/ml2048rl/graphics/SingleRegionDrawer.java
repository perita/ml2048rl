package com.peritasoft.ml2048rl.graphics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.ml2048rl.core.Direction;

class SingleRegionDrawer implements ActorDrawer {
    private final TextureRegion region;

    public SingleRegionDrawer(TextureRegion region) {
        this.region = region;
    }

    public SingleRegionDrawer(TextureAtlas atlas, String name) {
        this(atlas.findRegion(name));
        if (region == null) {
            throw new IllegalArgumentException("Region not found: " + name);
        }
    }

    @Override
    public void draw(Batch batch, float x, float y, Direction facing, float stateTime) {
        batch.draw(region, x, y);
    }
}
