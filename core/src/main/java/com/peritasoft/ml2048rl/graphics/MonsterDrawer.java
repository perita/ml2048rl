package com.peritasoft.ml2048rl.graphics;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.ml2048rl.core.Direction;

import java.util.HashMap;
import java.util.Map;

class MonsterDrawer implements ActorDrawer {
    private static final float FRAME_DURATION = 0.5f;

    private final Map<Direction, Animation<TextureRegion>> animations = new HashMap<>();

    public MonsterDrawer(TextureAtlas atlas, String name) {
        for (Direction direction : Direction.values()) {
            animations.put(direction, getAnimation(atlas, name, direction.index));
        }
    }

    private static Animation<TextureRegion> getAnimation(TextureAtlas atlas, String name, int index) {
        return new Animation<>(FRAME_DURATION,
                getRegion(atlas, name, index),
                getRegion(atlas, name, index + 1)
        );
    }

    private static TextureRegion getRegion(TextureAtlas atlas, String name, int index) {
        TextureRegion region = atlas.findRegion(name, index);
        if (region == null) {
            throw new IllegalArgumentException("region not found:" + name + "@" + index);
        }
        return region;
    }

    @Override
    public void draw(Batch batch, float x, float y, Direction facing, float stateTime) {
        TextureRegion region = animations.get(facing).getKeyFrame(stateTime, true);
        batch.draw(region, x, y);
    }
}
