package com.peritasoft.ml2048rl.graphics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.ml2048rl.core.Actor;
import com.peritasoft.ml2048rl.core.Direction;
import com.peritasoft.ml2048rl.core.Room;

class ActorSprite {
    public final Actor actor;
    public final ActorDrawer drawer;
    private final Vector2 position = new Vector2();
    private final Vector2 target;
    private ActorSprite mergedWith;
    private HocusPocus hocusPocus;

    public ActorSprite(Actor actor, ActorDrawer drawer, HocusPocus hocusPocus) {
        this.actor = actor;
        this.drawer = drawer;
        this.hocusPocus = hocusPocus;
        roomToPixels(this.position, actor.position);
        this.target = new Vector2(this.position);
    }

    public ActorSprite(Actor actor, ActorDrawer drawer, ActorSprite mergedTo) {
        this(actor, drawer, (HocusPocus) null);
        refresh();
        target.set(mergedTo.target);
    }

    private static float rowToY(int row) {
        return row * 16f + 32f;
    }

    private static float colToX(int col) {
        return col * 16f + 32f;
    }

    private static void roomToPixels(Vector2 pixelPosition, Room.Position roomPosition) {
        pixelPosition.set(colToX(roomPosition.col), rowToY(roomPosition.row));
    }

    public void act(float delta, float lerp) {
        position.lerp(target, lerp);
        if (hocusPocus != null) {
            hocusPocus.act(delta);
            if (hocusPocus.isDone()) {
                hocusPocus = null;
            }
        }
        if (mergedWith != null) {
            if (lerp > 0.5f) {
                mergedWith = null;
            } else {
                mergedWith.act(delta, lerp);
            }
        }
    }

    public void draw(Batch batch, AtlasFont font, Direction facing, float stateTime) {
        if (hocusPocus == null) {
            int value = actor.value;
            if (mergedWith != null) {
                mergedWith.draw(batch, font, facing, stateTime);
                value = mergedWith.actor.value;
            }
            drawer.draw(batch, getX(), getY(), facing, stateTime);
            font.drawCentered(batch, (int) Math.pow(2, value), getX() + 8, getY() + 6);
        } else {
            hocusPocus.draw(batch, getX(), getY());
        }
    }

    public void refresh() {
        hocusPocus = null;
        roomToPixels(this.position, actor.prevPosition);
        roomToPixels(this.target, actor.position);
        if (actor.mergedWith != null) {
            mergedWith = new ActorSprite(actor.mergedWith, drawer, this);
        }
    }

    public float getX() {
        return this.position.x;
    }

    public float getY() {
        return this.position.y;
    }

    public boolean outsideOfRoom() {
        return actor.room == null;
    }

    public boolean aboutToMove() {
        return actor.position.col != actor.prevPosition.col ||
                actor.position.row != actor.prevPosition.row ||
                (mergedWith != null && mergedWith.aboutToMove());
    }

    public boolean wasAttacked() {
        return actor.attacked;
    }

}
