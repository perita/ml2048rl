package com.peritasoft.ml2048rl.graphics;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

class AtlasFont {
    public static final float FONT_SIZE = 6f;
    private final TextureRegion[] digits;

    public AtlasFont(TextureAtlas atlas) {
        digits = new TextureRegion[10];
        for (int digit = 0; digit < digits.length; digit++) {
            digits[digit] = atlas.findRegion(String.valueOf(digit));
            if (digits[digit] == null) {
                throw new IllegalArgumentException("Digit not found: " + digit);
            }
        }
    }

    public void draw(Batch batch, int number, float x, float y) {
        draw(batch, String.valueOf(number), x, y);
    }

    public void drawCentered(Batch batch, int value, float x, float y) {
        String str = String.valueOf(value);
        draw(batch, str, x - str.length() * FONT_SIZE / 2f, y);
    }

    private void draw(Batch batch, CharSequence str, float x, float y) {
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            batch.draw(digits[c - '0'], x + FONT_SIZE * i, y - FONT_SIZE);
        }
    }

    public void drawPadded(Batch batch, int value, float x, float y, int length) {
        String str = String.valueOf(value);
        x -= str.length() * FONT_SIZE;
        draw(batch, str, x, y);
        while (length > str.length()) {
            x -= 6f;
            draw(batch, "0", x, y);
            length--;
        }
    }
}
