package com.peritasoft.ml2048rl.graphics;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

class HocusPocus {
    private final Animation<TextureRegion> animation;
    private float stateTime = 0.15f;

    HocusPocus(Animation<TextureRegion> animation) {
        this.animation = animation;
    }

    public void act(float delta) {
        stateTime = Math.max(stateTime - delta, 0f);
    }

    public boolean isDone() {
        return stateTime <= 0;
    }

    public void draw(Batch batch, float x, float y) {
        TextureRegion region = animation.getKeyFrame(stateTime);
        batch.draw(region, x, y);
    }
}
