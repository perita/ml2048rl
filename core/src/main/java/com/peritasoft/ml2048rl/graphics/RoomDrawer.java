package com.peritasoft.ml2048rl.graphics;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ScreenUtils;
import com.peritasoft.ml2048rl.core.*;

import java.util.HashSet;
import java.util.Set;

public class RoomDrawer implements Disposable, Actor.Visitor {
    private static final float BACKGROUND_COLOR = 54f / 255f;
    private static final float TORCH_FRAME_ANIMATION = 0.2f;
    private static final float SLASH_DURATION = 0.2f;

    private final TextureAtlas atlas = new TextureAtlas("ml2048rl.atlas");
    private final TextureRegion background = atlas.findRegion("background");
    private final ActorDrawer heroDrawer = new MonsterDrawer(atlas, "hero");
    private final ActorDrawer ratDrawer = new MonsterDrawer(atlas, "rat");
    private final ActorDrawer goblinDrawer = new MonsterDrawer(atlas, "goblin");
    private final ActorDrawer spiderDrawer = new MonsterDrawer(atlas, "spider");
    private final ActorDrawer goldDrawer = new SingleRegionDrawer(atlas, "gold");
    private final ActorDrawer potionDrawer = new SingleRegionDrawer(atlas, "potion");
    private final TextureRegion turn = atlas.findRegion("turn");
    private final Array<ActorDrawer> altarDrawers = createMultipleDrawers(atlas);
    private TextureRegion slain = null;

    private final Animation<TextureRegion> hocusPocusAnimation = new Animation<>(
            0.1f,
            atlas.findRegions("hocuspocus"),
            Animation.PlayMode.LOOP
    );
    private final Animation<TextureRegion> torchAnimation = new Animation<>(
            TORCH_FRAME_ANIMATION,
            atlas.findRegions("torch"),
            Animation.PlayMode.LOOP
    );
    private final Animation<TextureRegion> lightAnimation = new Animation<>(
            TORCH_FRAME_ANIMATION,
            atlas.findRegions("light"),
            Animation.PlayMode.LOOP
    );
    private final Animation<TextureRegion> slashAnimation = new Animation<>(
            0.1f,
            atlas.findRegions("slash")
    );
    private final AtlasFont font = new AtlasFont(atlas);
    private final Room room;
    private final Set<ActorSprite> actorSprites = new HashSet<>();
    private float lerp = 1f;
    private float slashTime;
    private final Array<Vector2> slashes = new Array<>(false, 16);
    private float stateTime;

    public RoomDrawer(Room room) {
        this.room = room;
        refresh();
    }

    public void act(float delta) {
        stateTime += delta;
        if (slashes.notEmpty()) {
            slashTime -= delta;
            if (slashTime <= 0f) {
                slashes.clear();
            }
        }
        lerp = Math.min(1f, lerp + delta * 4f);
        actorSprites.forEach(as -> as.act(delta, lerp));
    }

    public void refresh() {
        switch (room.state) {
            case INTERACTING:
                slashTime = 0f;
                actorSprites.forEach(as -> {
                    if (as.wasAttacked()) {
                        slashTime = SLASH_DURATION;
                        slashes.add(new Vector2(as.getX(), as.getY()));
                    }
                });
                // fall through
            case MOVING:
                lerp = 1f;
                actorSprites.removeIf(ActorSprite::outsideOfRoom);
                actorSprites.forEach(as -> {
                    as.refresh();
                    if (as.aboutToMove()) {
                        lerp = 0f;
                    }
                });
                break;
            case INVOKING:
                room.getActors().forEach(actor -> {
                    if (actor.justAdded) actor.visit(this);
                });
                break;
            case GAMEOVER:
                slain = atlas.findRegion("slain");
                break;
        }
    }

    public boolean isIdle() {
        return lerp > 0.5f && slashes.isEmpty();
    }

    private static Array<ActorDrawer> createMultipleDrawers(TextureAtlas actors) {
        final String name = "altar";
        Array<TextureAtlas.AtlasRegion> regions = actors.findRegions(name);
        if (regions.isEmpty()) {
            throw new IllegalArgumentException("No " + name + " found in atlas");
        }
        final Array<ActorDrawer> drawers = new Array<>(regions.size);
        regions.forEach(r -> drawers.add(new SingleRegionDrawer(r)));
        return drawers;
    }

    public void draw(Batch batch) {
        TextureRegion torch = torchAnimation.getKeyFrame(stateTime);
        TextureRegion light = lightAnimation.getKeyFrame(stateTime);
        ScreenUtils.clear(BACKGROUND_COLOR, BACKGROUND_COLOR, BACKGROUND_COLOR, 1f);
        batch.draw(background, 16f, 16f);
        batch.draw(torch, 48f, 128f);
        batch.draw(torch, 96f, 128f);
        batch.draw(light, 48f, 112f);
        batch.draw(light, 96f, 112f);
        actorSprites.forEach(as -> as.draw(batch, font, room.facing, stateTime));
        slashes.forEach(s -> drawSlash(batch, s));
        batch.draw(turn, 16f, 8f);
        font.draw(batch, room.turn, 46f, 14f);
        font.drawPadded(batch, room.score, 144f, 14f, 9);
        if (slain != null) {
            batch.draw(slain, 24f, 56f);
        }
    }

    @Override
    public void dispose() {
        atlas.dispose();
    }

    @Override
    public void accept(Hero hero) {
        addActorSprite(hero, heroDrawer);
    }

    @Override
    public void accept(Potion potion) {
        addActorSprite(potion, potionDrawer);
    }

    @Override
    public void accept(Rat rat) {
        addActorSprite(rat, ratDrawer);
    }

    private void drawSlash(Batch batch, Vector2 position) {
        TextureRegion region = slashAnimation.getKeyFrame(slashTime);
        batch.draw(region, position.x, position.y);
    }

    @Override
    public void accept(Goblin goblin) {
        addActorSprite(goblin, goblinDrawer);
    }

    @Override
    public void accept(Gold gold) {
        addActorSprite(gold, goldDrawer);
    }

    @Override
    public void accept(Spider spider) {
        addActorSprite(spider, spiderDrawer);
    }

    private void addActorSprite(Actor actor, ActorDrawer drawer) {
        actorSprites.add(new ActorSprite(actor, drawer, new HocusPocus(hocusPocusAnimation)));
    }

    @Override
    public void accept(Altar altar) {
        addActorSprite(altar, altarDrawers.random());
    }

}
